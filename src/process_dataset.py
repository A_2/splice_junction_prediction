'''
@author: ahmed allam <ahmed.allam@yale.edu>
'''
import os
import numpy as np
import pandas as pd
from sklearn.cross_validation import StratifiedKFold

current_dir = os.path.dirname(os.path.realpath(__file__))
root_dir = os.path.abspath(os.path.join(current_dir, os.pardir))
dataset_dir = os.path.join(root_dir, 'dataset')

def remove_trailingspaces(col):
    return(col.strip())

def expand_instance_toseq(df):
    target_col = 'obs'
    df[target_col] = df[target_col].apply(list)
    df_transformed = pd.DataFrame({col:np.repeat(df[col].values, df[target_col].str.len())
                                   for col in df.columns.difference([target_col])}).assign(**{target_col:np.concatenate(df[target_col].values)})[df.columns.tolist()]
    return(df_transformed)


def assign_label(d):
    gr = d.groupby('id')
    seq_info = {}
    # initialization
    for col in ('id', 'obs', 'label'):
        seq_info[col] = []
    for gr_id, gr_g in gr:
        fpart = gr_g['label'].iloc[0][0]
        if(fpart == 'I'):
            switch_label = 1
        elif(fpart == 'E'):
            switch_label = 2
        else:
            switch_label = 0
        seq_info['id']+= gr_g['id'].iloc[:].tolist()
        seq_info['obs']+= gr_g['obs'].iloc[:].tolist()
        label = np.repeat(0, 60).tolist()
        label[30] = switch_label
        seq_info['label']+= label
    return(pd.DataFrame(seq_info))

def write_seqs_tofile(df, fpath, sep="\t"):
    f_out = open(fpath, 'a')
    f_out.write(sep.join(df.columns.tolist()) + "\n")
    f_out.close()
    grouped_df = df.groupby(['id'])
    total_seqs = len(grouped_df)
    print("number of sequences is: ", total_seqs)
    for __, group_seq in grouped_df:
        group_seq.to_csv(fpath, mode='a', index=False, header=False, sep=sep, na_rep='NaN')
        f_out = open(fpath, 'a')
        f_out.write("\n")
        f_out.close()
        total_seqs -= 1
        print("{} sequences left".format(total_seqs))
        
def prepare_dataset():
    names = ['label', 'id', 'obs']
    converters = {'label':remove_trailingspaces, 'id':remove_trailingspaces,'obs':remove_trailingspaces}
    d = pd.read_csv(os.path.join(dataset_dir, 'splice.data'), sep=",", header=None, names=names, converters=converters)
    # after investigation, duplicates were found in the data
    d.drop_duplicates(['id'], inplace=True)
    d.reset_index(inplace=True, drop=False)
    ref_numinstances = d.shape[0]
    stratified_cv = StratifiedKFold(d['label'], n_folds=5)
    fold_c = 0
    for train_index, test_index in stratified_cv:
        print("train_index.shape ", train_index.shape)
        print("test_index.shape ", test_index.shape)
        num_instances = train_index.shape[0]+test_index.shape[0]
        print("sum of train and test ", num_instances)
        # check if the cross validation division result to total instances number
        assert num_instances == ref_numinstances
        # generate training fold data
        train_fold = d.loc[train_index]
        # transform the instance into sequence
        train_fold_transformed = expand_instance_toseq(train_fold)
        train_fold_transformed = assign_label(train_fold_transformed)
        write_seqs_tofile(train_fold_transformed[['id', 'obs', 'label']], os.path.join(dataset_dir, 'train_f_{}.txt'.format(fold_c)), sep = "\t")
        # generate testing fold data
        test_fold = d.loc[test_index]
        # transform the instance into sequence
        test_fold_transformed = expand_instance_toseq(test_fold)
        test_fold_transformed = assign_label(test_fold_transformed)
        write_seqs_tofile(test_fold_transformed[['id', 'obs', 'label']], os.path.join(dataset_dir, 'test_f_{}.txt'.format(fold_c)), sep = "\t")
        # increment fold counter
        fold_c += 1
        
        