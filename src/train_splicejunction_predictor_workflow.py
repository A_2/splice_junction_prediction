'''
@author: ahmed allam <ahmed.allam@yale.edu>
'''    
import os

from pyseqlab.features_extraction import HOFeatureExtractor
from pyseqlab.ho_crf_ad import HOCRFAD, HOCRFADModelRepresentation
from pyseqlab.workflow import GenericTrainingWorkflow
from pyseqlab.utilities import TemplateGenerator, create_directory, generate_trained_model,\
    generate_datetime_str
from splice_junction_attribute_extractor import  SpliceJunctionAttributeExtractor
import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score, classification_report, f1_score

current_dir = os.path.dirname(os.path.realpath(__file__))
root_dir = os.path.abspath(os.path.join(current_dir, os.pardir))
dataset_dir = os.path.join(root_dir, 'dataset', '5-fold')
        

def load_DataFileParser_options():
    data_parser_options = {}
    data_parser_options['header'] = 'main'
    data_parser_options['y_ref'] = True
    data_parser_options['column_sep'] = "\t"
    data_parser_options['seg_other_symbol'] = None
    return(data_parser_options)

def template_config():
    template_generator = TemplateGenerator()
    templateXY = {}
    template_generator.generate_template_XY('num_obs', ('1-gram:2-gram:3-gram:4-gram:5-gram:6-gram', range(-15, 16)), '1-state', templateXY)
    templateY = template_generator.generate_template_Y('2-states')
    return(templateXY, templateY)

def run_training(optimization_options, template_config, num_seqs=np.inf, profile=False):
    import cProfile
    if(profile):
        local_def = {'optimization_options':optimization_options,
                     'template_config':template_config,
                     'num_seqs':num_seqs
                    }
        global_def = {'train_crfs':train_crfs}
        uid = generate_datetime_str()
        profiling_dir = create_directory('profiling', root_dir)
        cProfile.runctx('train_crfs(optimization_options, template_config, num_seqs=num_seqs)',
                        global_def, local_def, filename = os.path.join(profiling_dir, "timeprofile_{}".format(uid)))
    else:
        return(train_crfs(optimization_options, template_config, num_seqs=num_seqs))

def revive_learnedmodel(model_dir):
    lmodel = generate_trained_model(os.path.join(model_dir, 'model_parts'), SpliceJunctionAttributeExtractor)
    return(lmodel)

def predict_label_sum(d):
    # check if the id column is correct
    if(len(d.loc[0, 'id']) <= 1):
        d.columns = ['obs', 'id', 'ref', 'pred']
    gr = d.groupby('id')
    seq_info = {}
    # initialization
    for col in ('id', 'obs', 'ref', 'pred'):
        seq_info[col] = []
    for gr_id, gr_g in gr:
        seq_info['obs'].append("".join(gr_g['obs'].tolist()))
        seq_info['ref'].append(gr_g['ref'].sum())
        seq_info['pred'].append(gr_g['pred'].sum())
        seq_info['id'].append(gr_id)
    return(pd.DataFrame(seq_info))

def predict_label_concat(d):
    # check if the id column is correct
    if(len(d.loc[0, 'id']) <= 1):
        d.columns = ['obs', 'id', 'ref', 'pred']
    gr = d.groupby('id')
    seq_info = {}
    # initialization
    for col in ('id', 'obs', 'ref', 'pred'):
        seq_info[col] = []
    for gr_id, gr_g in gr:
        seq_info['obs'].append("".join(gr_g['obs'].tolist()))
        seq_info['ref'].append("".join([str(elem) for elem in gr_g['ref'].tolist()]))
        seq_info['pred'].append("".join([str(elem) for elem in gr_g['pred'].tolist()]))
        seq_info['id'].append(gr_id)
    return(pd.DataFrame(seq_info))

def get_top_features(crf_m, y_pattern, n):
    w = crf_m.weights
    m_features_codebook = crf_m.model.modelfeatures_codebook
    df_codebook = pd.DataFrame([[feature, indx] for feature, indx in m_features_codebook.items()])
    df_codebook.columns = ['feature', 'indx']
    df_codebook.set_index('indx', drop=False, inplace=True)
    m_features = crf_m.model.modelfeatures[y_pattern]
    target_indx = []
    for feature_name in m_features:
        target_indx.append(m_features_codebook[(y_pattern,feature_name)])
    w_target = w[target_indx]
    t = np.array(target_indx)
    windx_sorted = t[np.argsort(-w_target)]
    for feature in df_codebook.loc[windx_sorted[:n]]['feature']:
        print(feature)
        
def get_performance(df, **kwargs):
    #df = pd.read_csv(decseq_file, header=0, sep="\t")
    cols = df.columns.tolist()
    other_symbol = kwargs.get('other_symbol')
    iob_repr = False
    if(type(other_symbol) != type(None)):
        iob_repr = True
        
    if(iob_repr):
        y_pred = df[cols[-1]].str.split("-", expand=True)[1]
        y_pred.loc[y_pred.isnull()] = other_symbol 
        y_pred = y_pred.astype('int')
    else:
        y_pred = df[cols[-1]]
    y_pred = y_pred.tolist()
    if(iob_repr):
        y_ref = df[cols[-2]].str.split("-", expand=True)[1]
        y_ref.loc[y_ref.isnull()] = other_symbol
        y_ref = y_ref.astype('int')
    else:
        y_ref = df[cols[-2]]
    y_ref = y_ref.tolist()
    print(classification_report(y_ref, y_pred))
    print("weighted f1:")
    print(f1_score(y_ref, y_pred, average='weighted'))
    print("micro f1:")
    print(f1_score(y_ref, y_pred, average='micro'))
    
def eval_models(models_dir):
    error_score= []
    for i in range(len(models_dir)):  
        train_fname = "train_fold_{}.txt".format(i)
        test_fname = "test_fold_{}.txt".format(i)
        for f in (train_fname, test_fname):
            print(f)
            model_dir = models_dir[i]
            file_path = os.path.join(model_dir, "decoding_seqs", f)
            score = eval_decoded_file(file_path)
            if('test' in f):
                error_score.append(score)
    return(error_score)

def eval_decoded_file(fpath):
    d = pd.read_csv(fpath, header=None, sep="\t")
    d.columns = ['id', 'obs', 'ref', 'pred']
    d_c = predict_label_concat(d)
    get_performance(d_c)
    return(1-accuracy_score(d_c['ref'], d_c['pred']))
          
def train_crfs(optimization_options, template_config, num_seqs=np.inf):
    crf_model = HOCRFAD 
    model_repr = HOCRFADModelRepresentation
    
    # init attribute extractor
    spljunc_attrextractor = SpliceJunctionAttributeExtractor()
    # get the attribute description 
    attr_desc = spljunc_attrextractor.attr_desc
    
    # load templates
    template_XY, template_Y = template_config()
    # init feature extractor
    fextractor = HOFeatureExtractor(template_XY, template_Y, attr_desc)
    # no need for feature filter
    fe_filter = None
    # load data/file parser options
    data_parser_options = load_DataFileParser_options()
    # create a root directory for processing the data
    wd = create_directory('wd', root_dir)
    workflow_trainer = GenericTrainingWorkflow(spljunc_attrextractor, fextractor, fe_filter, 
                                               model_repr, crf_model,
                                               wd)
    # define the data split strategy
    # since data is alread organized in separate files (train and test)
    # therefore we use all data in training file 
    dsplit_options = {'method':"none"}
    # since we are not going to train using perceptron, we can safely setup full_parsing=False
    full_parsing = False
    models_perf = []
    perf_metric = 'f1'
    for fold_c in range(0, 5):
        # load data_parser_options
        data_parser_options = load_DataFileParser_options()
        # load train file
        train_file = os.path.join(dataset_dir, 'train_f_{}.txt'.format(fold_c))
        data_split = workflow_trainer.seq_parsing_workflow(dsplit_options,
                                                           seq_file=train_file,
                                                           data_parser_options=data_parser_options,
                                                           num_seqs=num_seqs,
                                                           full_parsing = full_parsing)
        trainseqs_id = data_split[0]['train']
        crf_m = workflow_trainer.build_crf_model(trainseqs_id,
                                                "f_{}".format(fold_c), 
                                                full_parsing=full_parsing)
        model_dir = workflow_trainer.train_model(trainseqs_id, 
                                                 crf_m, 
                                                 optimization_options)
        use_options_seqsinfo = {'seqs_info':workflow_trainer.seqs_info,
                                'model_eval':True,
                                'metric':perf_metric,
                                'file_name':'train_fold_{}.txt'.format(fold_c),
                                'sep':'\t'
                               }
        model_train_perf = workflow_trainer.use_model(model_dir, use_options_seqsinfo)  
        # evaluate on train file          
        print("{} performance on train_f_{}".format(perf_metric, fold_c))
        print(model_train_perf[perf_metric]) 
        
        # get test file
        test_file = os.path.join(dataset_dir, 'test_f_{}.txt'.format(fold_c))
        # define the options to use the model while providing a sequence file
        use_options_seqfile = {'seq_file':test_file,
                               'data_parser_options':data_parser_options,
                               'num_seqs':num_seqs,
                               'model_eval':True,
                               'metric':perf_metric,
                               'file_name':'test_fold_{}.txt'.format(fold_c),
                               'sep':'\t'
                            }

        model_test_perf = workflow_trainer.use_model(model_dir, use_options_seqfile)
        # evaluate on train file          
        print("{} performance on test_f_{}".format(perf_metric, fold_c))
        print(model_test_perf[perf_metric]) 

        models_perf.append((model_dir, model_train_perf, model_test_perf))
        print("-"*50)
    return(models_perf)
  
if __name__ == "__main__":
    pass
    
    
