'''
@author: ahmed allam <ahmed.allam@yale.edu>
'''
from pyseqlab.attributes_extraction import GenericAttributeExtractor

class SpliceJunctionAttributeExtractor(GenericAttributeExtractor):
    """class implementing observation functions that generates attributes from observations"""

    def __init__(self):
        attr_desc = self.generate_attributes_desc()
        super().__init__(attr_desc)
    
    def generate_attributes_desc(self):
        attr_desc = {}
        attr_desc['obs'] = {'description': 'nucleotides observation',
                            'encoding':'categorical'
                           }
        attr_desc['num_obs'] = {'description': 'numbered nucleotides observation  (i.e. nucleotide letter concatenated with the position)',
                                'encoding':'categorical'
                               }

        return(attr_desc)
    
    def generate_attributes(self, seq, boundaries):
        X = seq.X  
        observed_attrnames = ['obs']
#         observed_attrnames = list(X[1].keys())
        # segment attributes dictionary
        self.seg_attr = {}
        new_boundaries = []
        # create segments from observations using the provided boundaries
        for boundary in boundaries:
            if(boundary not in seq.seg_attr):
                self._create_segment(X, boundary, observed_attrnames)
                new_boundaries.append(boundary)
#         print("seg_attr {}".format(self.seg_attr))
#         print("new_boundaries {}".format(new_boundaries))
        if(self.seg_attr):
            for boundary in new_boundaries:
                self.get_numpos_plus_obs(boundary)
            # save generated attributes in seq
            seq.seg_attr.update(self.seg_attr)
#             print('saved attribute {}'.format(seq.seg_attr))
            # clear the instance variable seg_attr
            self.seg_attr = {}
        return(new_boundaries)
    
    def get_numpos_plus_obs(self, boundary):
        obs_attr = 'obs'
        numpos_obs_attr = 'num_obs'
        u, __ = boundary
        self.seg_attr[boundary][numpos_obs_attr] = self.seg_attr[boundary][obs_attr] + str(u)
        
def example():
    from pyseqlab.utilities import DataFileParser
    import os
    current_dir = os.path.dirname(os.path.realpath(__file__))
    root_dir = os.path.abspath(os.path.join(current_dir, os.pardir))
    dataset_dir = os.path.join(root_dir, "dataset")
    # initialize a data file parser
    dparser = DataFileParser()
    # provide the options to parser such as the header info, the separator between words and if the y label is already existing
    # main means the header is found in the first line of the file
    header = "main"
    # y_ref is a boolean indicating if the label to predict is already found in the file
    y_ref = True
    # spearator between the words/observations
    column_sep = "\t"
    seqs = []
    # read only one sequence
    for seq in dparser.read_file(os.path.join(dataset_dir, "5-fold", "train_f_0.txt"), header, y_ref=y_ref, column_sep = column_sep):
        seqs.append(seq)
        break
    attr_extractor = SpliceJunctionAttributeExtractor()
    print("attr_desc {}".format(attr_extractor.attr_desc))
    attr_extractor.generate_attributes(seq, seq.get_y_boundaries())
    for boundary, seg_attr in seq.seg_attr.items():
        print("boundary {}".format(boundary))
        print("attributes {}".format(seg_attr))
    print("seg_attr {}".format(seq.seg_attr))
    return(seq)

if __name__ == "__main__":
    example()
